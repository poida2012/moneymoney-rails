json.array!(@accounts) do |account|
  json.extract! account, :id, :name, :color
  json.url account_url(account, format: :json)
end
